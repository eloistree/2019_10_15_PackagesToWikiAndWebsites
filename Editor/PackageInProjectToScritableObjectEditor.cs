﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PackageInProjectToScritableObject))]
public class PackageInProjectToScritableObjectEditor : Editor
{
    public static bool m_fullEditor;

    public override void OnInspectorGUI()
    {
        PackageInProjectToScritableObject myScript = (PackageInProjectToScritableObject)target;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Refresh"))
        {
            myScript.Refresh();
        }
        if (GUILayout.Button("Override Packages Files"))
        {
            myScript.OverridePackagesWithNewInformation();
        }
        if (GUILayout.Button("Save in Scriptable"))
        {
            myScript.SaveInScritpableObject();
        }
        GUILayout.EndHorizontal();
        if (m_fullEditor =EditorGUILayout.Foldout(m_fullEditor, "Edit name and description"))
        {
            for (int i = 0; i < myScript.m_gitPackageInAssetsFolder.Count; i++)
            {
                GUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Name:", GUILayout.Width(80));
                myScript.m_gitPackageInAssetsFolder[i].m_packageInfo.displayName = EditorGUILayout.TextField(myScript.m_gitPackageInAssetsFolder[i].m_packageInfo.displayName);
                GUILayout.EndHorizontal();

                myScript.m_gitPackageInAssetsFolder[i].m_packageInfo.description = EditorGUILayout.TextArea(myScript.m_gitPackageInAssetsFolder[i].m_packageInfo.description);
                EditorGUILayout.Space();

            }
        }
        
            DrawDefaultInspector();



    }

}
