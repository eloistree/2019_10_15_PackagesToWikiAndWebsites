﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackagesInfoToPatreon : MonoBehaviour
{

    public PackagesInformationObject m_packages;
    [TextArea(1, 4)]
    public string m_startBlock = "<div>";
    [TextArea(1, 4)]
    public string m_formatPerLine = "<span style=\"font-size: 60%;color: rgb(192, 80, 77);\"><a href=\"#GITLINK#\"> #IDNAME#</a>: #DESCRIPTION# </br></span>\n\r";
    [TextArea(1, 4)]
    public string m_endBlock = "</div>";
    [TextArea(0,30)]
    public string m_patreonDisplay;


    private void OnValidate()
    {
        if (m_packages == null)
            return;
        m_patreonDisplay = "";
        string format = m_formatPerLine;
        format = format.Replace("#GITLINK#", "{0}");
        format = format.Replace("#IDNAME#", "{1}");
        format = format.Replace("#DESCRIPTION#", "{2}");
        format = format.Replace("#NAMESPACE#", "{3}");

        m_patreonDisplay = m_startBlock;
        foreach (GitUnityPackageLink item in m_packages.m_data.m_packages)
        {
            m_patreonDisplay += string.Format(format, item.m_gitLink, item.m_packageInfo.GetDisplayName(), item.m_packageInfo.GetDescriptionVersion(), item.m_packageInfo.GetNamespaceID());
        }

        m_patreonDisplay +=m_endBlock;
    }
}
