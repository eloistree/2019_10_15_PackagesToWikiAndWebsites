﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PackagesInformationObject : ScriptableObject
{
    public PackagesCollection m_data;
}

[System.Serializable]
public class PackagesCollection {
    public List<GitUnityPackageLink> m_packages;

}
