﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackagesInfoToWiki : MonoBehaviour
{

    public PackagesInformationObject m_packages;
    [TextArea(1, 4)]
    public string m_startBlock = "## Packages List\n ";
    [TextArea(1, 4)]
    public string m_formatPerLine = "- [\"#NAMESPACE#\": #IDNAME#](#GITLINK#)\n  ";
    [TextArea(1, 4)]
    public string m_endBlock = "[More Information](http://google.com)\n    ";
    [TextArea(0,30)]
    public string m_patreonDisplay;


    private void OnValidate()
    {
        if (m_packages == null)
            return;
        m_patreonDisplay = "";
        string format = m_formatPerLine;
        format = format.Replace("#GITLINK#", "{0}");
        format = format.Replace("#IDNAME#", "{1}");
        format = format.Replace("#DESCRIPTION#", "{2}");
        format = format.Replace("#NAMESPACE#", "{3}");

        m_patreonDisplay = m_startBlock;
        foreach (GitUnityPackageLink item in m_packages.m_data.m_packages)
        {
            m_patreonDisplay += string.Format(format, item.m_gitLink, item.m_packageInfo.GetDisplayName(), item.m_packageInfo.GetDescriptionVersion(), item.m_packageInfo.GetNamespaceID());
        }
        m_patreonDisplay += m_endBlock;

    }
}
