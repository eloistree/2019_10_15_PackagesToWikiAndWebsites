﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PackageInProjectToScritableObject : MonoBehaviour
{

    public PackagesInformationObject m_packagesInformation;
    Utility_ManifestJson m_currentManifest;
    List<DependencyJson> m_packageDependency = new List<DependencyJson>();
    public List<GitUnityPackageLinkOnDisk> m_gitPackageInAssetsFolder = new List<GitUnityPackageLinkOnDisk>();
    [TextArea(2, 20)]
    public string m_packagesList;





    public void Refresh()
    {
        m_currentManifest = UnityPackageUtility.GetManifest();
        foreach (DependencyJson item in m_currentManifest.dependencies)
        {
            string link;
            bool hasLink = item.GetLink(out link);
            if (hasLink)
                m_packageDependency.Add(item);
        }

        m_gitPackageInAssetsFolder = UnityPackageUtility.GetGitUnityPackageInDirectory(Application.dataPath);

        m_packagesList = "";
        for (int i = 0; i < m_gitPackageInAssetsFolder.Count; i++)
        {
            if (m_gitPackageInAssetsFolder[i].m_packageInfo != null)
                m_packagesList += string.Format("\"{0}\":\"{1}\",\n", m_gitPackageInAssetsFolder[i].m_packageInfo.GetNamespaceID(), m_gitPackageInAssetsFolder[i].m_gitLink);
            else Debug.LogWarning("The folder is a git folder but don't have a package.json... Or something like that.");
        
        }
    }

    public void OverridePackagesWithNewInformation()
    {
        for (int i = 0; i < m_gitPackageInAssetsFolder.Count; i++)
        {
          //  Debug.Log(m_gitPackageInAssetsFolder[i].m_projectDirectoryPath + "/package.json");
            UnityPackageUtility.SetPackageFile(m_gitPackageInAssetsFolder[i].m_projectDirectoryPath + "/package.json", m_gitPackageInAssetsFolder[i].m_packageInfo);

        }
    }


    public void SaveInScritpableObject() {
        for (int i = 0; i < m_gitPackageInAssetsFolder.Count; i++)
        {
            m_packagesInformation.m_data.m_packages.Add( m_gitPackageInAssetsFolder[i]);
        }
    }

    private void Reset()
    {

        Refresh();
    }
}
